var canvas;
var context;
var touchTotal   = 0;
var touchCurrent = 0;

function resizeCanvas() {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;

  update();
}

function update(){
  canvas.width = canvas.width;

  context.fillStyle = "#ff0000";
  context.font = canvas.height/2+"px Arial";
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.fillText(touchTotal, canvas.width/2, canvas.height / 2);
}

function delay(){
  var i=2500;
  while(i--);
}

function inc(e){
  console.log("inc");
  touchCurrent++;
  if(touchCurrent > touchTotal) touchTotal = touchCurrent;

  update();
  delay();
}

function dec(e){
  console.log("dec");
  touchCurrent--;

  update();
  delay();
}

function main(){
  canvas = document.querySelector("#canvas");
  context = canvas.getContext("2d");

  document.body.addEventListener('touchstart', inc, false);
  document.body.addEventListener('touchend', dec, false);

  window.addEventListener('resize', resizeCanvas, false);
  resizeCanvas();
}

main();
